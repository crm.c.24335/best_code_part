import './registrationForm.scss';
import AppButton from '../../components/AppButton/AppButton';
import RadioInput from '../UI/RadioInput/RadioInput';
import TextInput from '../UI/TextInput/TextInput';
import FileInput from '../UI/FileInput/FileInput';
import Preloader from '../Preloader/Preloader';
import ErrorMessage from '../ErrorMessage/ErrorMessage';
import DataNotFoundMessage from '../DataNotFoundMessage/DataNotFounMessage';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { validationPatterns } from './validationPatterns';
import { useDispatch, useSelector } from 'react-redux';
import { positions, loadingStatus } from '../../redux/userPositions/selectors';
import { getPositions } from '../../redux/sagas/loadUsersPositionsService/actions';
import { createNewUser } from '../../redux/sagas/postNewUserService/actions';
import {
    userLoadingStatus,
    errorMessage,
    fails,
} from '../../redux/user/selectors';
import { validateImage } from './validationPatterns';

const RegistrationForm = () => {
    //selectors
    const dispatch = useDispatch();
    const { emailValidate, nameValidate, phoneValidate } = validationPatterns();
    const usersPositions = useSelector(positions);
    const userPositionsLoadingStatus = useSelector(loadingStatus);
    const createUserStatus = useSelector(userLoadingStatus);
    const createNewUserErrorMessage = useSelector(errorMessage);
    const responseValidationFailMessages = useSelector(fails);

    //React Hook Form Hooks
    const {
        register,
        handleSubmit,
        formState: { errors, isValid },
        reset,
        watch,
        setError,
        clearErrors,
    } = useForm({ mode: 'all' });

    //on form submit function
    const onSubmit = data => {
        const formData = new FormData();
        for (let key in data) {
            if (typeof data[key] === 'object') {
                formData.append(key, data[key][0]);
            } else {
                formData.append(key, data[key]);
            }
        }
        dispatch(createNewUser(formData));
        reset();
    };

    //get initial user position on page start
    useEffect(() => {
        dispatch(getPositions());
    }, [dispatch]);

    //create positions list
    const positionsList = usersPositions.map((user, idx) => {
        return (
            <div className="form-group__item form-group__item_mb7" key={idx}>
                <RadioInput
                    label={user.name}
                    register={{
                        ...register('position_id', { required: true }),
                    }}
                    value={user.id}
                    id={user.id}
                />
            </div>
        );
    });

    return (
        <form className="form" onSubmit={handleSubmit(onSubmit)}>
            <div className="form-group form-group_mb29">
                <div className="form-group__item form-group__item_mb36">
                    <TextInput
                        type="text"
                        errors={errors}
                        register={{
                            ...register('name', nameValidate),
                        }}
                        label="Your name"
                        value={watch('name')}
                    />
                </div>
                <div className="form-group__item form-group__item_mb36">
                    <TextInput
                        type="text"
                        errors={errors}
                        label="Email"
                        register={{ ...register('email', emailValidate) }}
                        value={watch('email')}
                    />
                </div>

                <div className="form-group__item form-group__item_mb36">
                    <TextInput
                        type="tel"
                        errors={errors}
                        label="Phone"
                        register={{ ...register('phone', phoneValidate) }}
                        value={watch('phone')}
                    />
                </div>
            </div>
            <div className="form-group form-group_mb44">
                <span className="form__position">Select your position</span>
                {userPositionsLoadingStatus !== 'error' ? positionsList : null}
                <DataNotFoundMessage
                    status={userPositionsLoadingStatus}
                    data={positionsList}
                    message="No positions yet."
                />
                <Preloader status={userPositionsLoadingStatus} />
                <ErrorMessage status={userPositionsLoadingStatus} />
            </div>
            <div className="form-group form-group_mb50">
                <FileInput
                    title="Upload your photo"
                    label="Upload your photo"
                    buttonText="Upload"
                    accept="image/jpg, image/jpeg"
                    errors={errors}
                    type="file"
                    register={{
                        ...register('photo', {
                            required: true,
                            validate: file =>
                                validateImage(
                                    file,
                                    clearErrors,
                                    setError,
                                    'photo'
                                ),
                        }),
                    }}
                />
            </div>
            <Preloader status={createUserStatus} />
            <ErrorMessage
                status={createUserStatus}
                isMessage={true}
                message={createNewUserErrorMessage}
            />
            <ErrorMessage
                status={createUserStatus}
                isMessage={true}
                message={responseValidationFailMessages}
            />

            <AppButton
                text="Sign up"
                type="submit"
                center={true}
                disabled={!isValid}
                hide={createUserStatus === 'loading'}
            />
        </form>
    );
};

export default RegistrationForm;
